	<!-- Test Commit -->
<html>
<head>	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.js"></script>
</head>

<body>
	<div class="col-lg-12" style="border: 1px solid black;">
		<div class="col-lg-12">
			<?php 
				if(ISSET($_GET['act'])) {
					$act = $_GET['act'];
					$username = $_GET['username'];
					$mysqli = new mysqli("localhost", "root", "mysql", "webdeveloper");
					if($act == "edit") {
						$stmt = $mysqli->prepare("SELECT * FROM user WHERE username = ?");
						$stmt->bind_param("s", $username);
						$stmt->execute();
						//fetching result would go here, but will be covered later
						$result = $stmt->get_result();
						while($row = $result->fetch_assoc()) {
        					?>
        					<form action="index.php" id="formRegister" method="POST">
			  <div class="form-group">
			    <label for="inputUsername">Username</label>
			    <input type="text" class="form-control" id="inputUsername" name="inputUsername" placeholder="Masukkan username" value="<?php echo $row['username']; ?>" readonly = "readonly">
			  </div>
			  <div class="form-group">
			    <label for="inputNama">Nama</label>
			    <input type="text" class="form-control" id="inputNama" name="inputNama" placeholder="Masukkan nama" value="<?php echo $row['nama']; ?>">
			  </div>
			  <div class="form-group">
			    <label for="inputEmail">Email</label>
			    <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Masukkan email" value="<?php echo $row['email']; ?>">
			  </div>
			  <button type="submit" name="buttonSubmit" id="buttonSubmit" class="btn btn-primary">Simpan</button>
			</form>
        					<?php 
    					}
						$stmt->close();
					}
					else if($act == "delete") {
						$stmt = $mysqli->prepare("DELETE FROM user WHERE username = ?");
						$stmt->bind_param("s", $username);
						$stmt->execute();
						$stmt->close();
						header('Location: '."index.php");
					}
				}
				else {
				?>
			<form action="index.php" id="formRegister" method="POST">
			  <div class="form-group">
			    <label for="inputUsername">Username</label>
			    <input type="text" class="form-control" id="inputUsername" name="inputUsername" placeholder="Masukkan username">
			  </div>
			  <div class="form-group">
			    <label for="inputNama">Nama</label>
			    <input type="text" class="form-control" id="inputNama" name="inputNama" placeholder="Masukkan nama">
			  </div>
			  <div class="form-group">
			    <label for="inputEmail">Email</label>
			    <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Masukkan email">
			  </div>
			  <button type="submit" name="buttonSubmit" id="buttonSubmit" class="btn btn-primary">Simpan</button>
			</form>
			<?php } ?>
			<?php 
				if(ISSET($_POST['buttonSubmit'])) {
					$mysqli = new mysqli("localhost", "root", "mysql", "webdeveloper");
					$nama = $mysqli->real_escape_string($_POST['inputNama']);
					$username = $mysqli->real_escape_string($_POST['inputUsername']);
					$email = $mysqli->real_escape_string($_POST['inputEmail']);

					if(!empty($nama) && !empty($username) && !empty($email)) {
						$newData = false;
						$stmt = $mysqli->prepare("SELECT * FROM user WHERE username = ? ");
						$stmt->bind_param("s", $username);
						$stmt->execute();
						//fetching result would go here, but will be covered later
						$result = $stmt->get_result();
						if($result->num_rows === 0)
							$newData = true;
						$stmt->close();

						if($newData) {
							$stmt = $mysqli->prepare("INSERT INTO user (username, nama, email) VALUES (?, ?, ?)");
							$stmt->bind_param("sss", $username, $nama, $email);
							$stmt->execute();
							$stmt->close();
						}
						else {
							$stmt = $mysqli->prepare("UPDATE user SET nama = ?, email = ? WHERE username = ?");
							$stmt->bind_param("sss", $nama, $email, $username);
							$stmt->execute();
							$stmt->close();
						}
					}
				}
			?>
		</div>
		<div class="col-lg-12 table-responsive" id="divTable">
			<table class="table table-stripped" border="1">
				<thead>
					<tr>
						<th>No</th>
						<th>Username</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					//Read Data 
					$mysqli = new mysqli("localhost", "root", "mysql", "webdeveloper");
					$stmt = $mysqli->prepare("SELECT * FROM user");
					$stmt->execute();
					//fetching result would go here, but will be covered later
					$result = $stmt->get_result();
					$no = 1;
					while($row = $result->fetch_assoc()) {
						echo "<tr>";
						echo "<td>".$no."</td>";
						echo "<td>".$row['username']."</td>";
						echo "<td>".$row['nama']."</td>";
						echo "<td>".$row['email']."</td>";
						echo "<td>
						<a href='index.php?act=edit&username=".$row['username']."'>Edit</a>
						<a href='index.php?act=delete&username=".$row['username']."'>Delete</a>

						</td>";
						echo "</tr>";
						$no++;
					}
					$stmt->close();
					?>
				</tbody>
			</table>
		</div>
	<div>



	<script>
		$(document).ready(function() {
			$("#formRegister").validate({

			rules:{
				'inputUsername':{
				required: true,
				minlength: 1
				},

				'inputNama':{
				required: true,
				minlength: 3,
				// username_regex: true,

				// remote:{

				// url: "validatorAJAX.php",

				// type: "post"

				// }

				},
				'inputEmail':{
				required: true,
				email: true,
				}
			},
			messages:{
				'inputUsername':{
				required: "Username harus diisi",
				minlength: "Choose a username of at least 1 letters!",
				},

				'inputNama':{
				required: "Nama harus diisi",
				minlength: "Choose a username of at least 4 letters!",

				},

				'inputEmail':{
				required: "Email harus diisi",
				email: "Please enter a valid email address!",
				},
			},

		    submitHandler: function(form) {
		        form.submit();
		    },


		});

		});

	</script>
</body>
</html>